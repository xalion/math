# Вычисление квадратов чисел по Гансу Бете
# Метод работает до пятидесяти включительно


def trick_Hans_Bethe(num_to_power):
    limit_calculate_extent = 50

    # Разность между пятьюдесятью и числом для возведения в квадрат
    difference_limit_and_num = limit_calculate_extent - num_to_power

    # Из 50² вычитаем разность сто раз
    result_calculated = 50 ** 2 - difference_limit_and_num * 100

    # Для точного ответа к результату вычитания прибавляем квадрат разности
    result_calculated += difference_limit_and_num ** 2
    return result_calculated
